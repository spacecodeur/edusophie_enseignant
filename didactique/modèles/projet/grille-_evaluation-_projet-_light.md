# Grille d'évaluation de projet

<table class="table table-sm">
  <tbody>
    <tr>
      <td><b>Nom du projet</b></td>
      <td class="col-8"></td>
    </tr>
  </tbody>
</table>
  

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center" colspan="4">Groupe</th>
    </tr>
    <tr class="text-center align-middle">
      <th class="col-1">#</th>
      <th class="col-3">Nom Prenom</th>
      <th class="col-3">Principale(s) contribution(s) dans le projet (si il y a plusieurs membres dans le groupe)</th>
  </tr></thead>
  <tbody>
    <tr>
      <td class="p-3 text-center"><b>M1</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M2</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M3</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

## Évaluation orale

<div class="col-10 mx-auto mb-4">
  <table class="table table-sm">
    <thead>
      <tr>
        <th colspan="4" class="text-center">Coefficients</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <tr>
        <td class="col-3"><b>N</b><sub>on</sub> <b>A</b><sub>quis</sub> | non</td>
        <td class="col-3"><b>P</b><sub>assable</sub></td>
        <td class="col-3"><b>B</b><sub>ien</sub></td>
        <td class="col-3"><b>T</b><sub>rès</sub><b>B</b><sub>ien</sub> | oui</td>
      </tr>
      <tr>
        <td>0</td>
        <td>0.3</td>
        <td>0.7</td>
        <td>1</td>
      </tr>
    </tbody>
  </table>
</div>

<div class="col-10 mx-auto mb-4">
  <table class="table table-sm">
    <thead>
      <tr>
        <th colspan="4" class="text-center">Temps de présentation total (hors démo) en fonction du nombre d'intervenants dans le projet</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <tr>
        <td>Projet individuel</td>
        <td>Binôme</td>
        <td>Groupe de 3 personnes</td>
      </tr>
      <tr>
        <td>5 minutes</td>
        <td>6 minutes</td>
        <td>8 minutes</td>
      </tr>
    </tbody>
  </table>
</div>

<hr class="my-5">

<table class="table table-sm">
  <thead>
    <tr>
      <th colspan="6" class="text-center">Qualité de la présentation (15 points communs)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2"><ul>
        <li>Le temps de présentation total est respecté</li>
        <li>La présentation (préparée en amont) est organisée, claire et fluide</li>
        <li>Dans le cadre d'un projet en groupe, le temps de parole est équitablement réparti</li>
      </ul></td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;height:23px !important">NA</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">P</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">B</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>5 points</b></td>
      <td rowspan="2">Une démonstration du projet est faîte : <ul>
        <li>Le temps de démonstration ne doit pas excéder les 2 minutes</li>
        <li>Aucune erreur ne doit apparaître pendant la démonstration</li>
      </ul></td>
      <td class="text-center" style="height:23px !important">NA</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2">L'application résiste à un cas de test de valeur limite (énoncé par le formateur et/ou le public)</td>
      <td class="text-center" style="height:23px !important">NA</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>4 points</b></td>
      <td rowspan="2">Le code de l'application est propre et facilement lisible :<ul>
        <li>Un travail a été apporté sur la structuration globale du projet</li>
        <li>le code est commenté, surtout les parties qui ont pu poser des difficultés</li>
        <li>le nom des variables et des fonctions est clair</li>
      </ul></td>
      <td class="text-center" style="height:23px !important">NA</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center " colspan="6">Questions techniques individuelles (5 points)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>5 points</b></td>
      <td rowspan="2">Chaque intervenant dans le projet a su répondre à une question technique</td>
      <td class="text-center" style="flex: 0 0 auto;width: 5.555555%;height:23px !important">M1</td>
      <td class="text-center" style="flex: 0 0 auto;width: 5.555555%;">M2</td>
      <td class="text-center" style="flex: 0 0 auto;width: 5.555555%;">M3</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

## Bilan

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th style="border-left-style:hidden;border-top-style:hidden;"></th>
      <th>M1</th>
      <th>M2</th>
      <th>M3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Qualité de la présentation (sur 15 points)</td>
      <td colspan="4"></td>
    </tr>
    <tr>
      <td>Question technique individuelle (sur 5 points)</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="border-2">
      <td>Note finale sur 20</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

