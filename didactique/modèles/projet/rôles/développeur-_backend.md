# Rôle : développeur(se) backend 

+++image "/images/md/D5G69B8CBGEH5FFH332C273HEG9BH9" col-4 mx-auto

+++citation
  Dans l'industrie informatique, le(la) développeur(se) backend est un(e) informaticien(e) spécialisé(e) dans le traitement et le transfert des données. Avec de l'expérience, cet(te) architecte des données est capable de concevoir et de développer des systèmes d'informations complexes. 
+++

<div class="text-center h3 my-4">Spécialités du développeur frontend <b>au lycée</b></div>

<ul>
  <li>Concevoir et implémenter des algorithmes</li>
  <li>Contrôler et traiter des informations</li>
  <li>Déployer un serveur Web</li>
</ul>

<div class="text-center h3 my-4">Technologies utilisées <b>au lycée</b></div>

- **langages** : python, csv, json
- **formats de fichiers utilisés** : py, csv, json
- <abbr title="un framework est une sorte de boîte à outils informatiques qui aide à développer une application"><b>frameworks</b></abbr> : 
    - flask (python) : création de site ou application Web

<div class="text-center h3 my-4">Arbre de progression <b>niveau lycée et au-delà</b></div>

+++bulle matthieu
  étant donné l'émergence quotidienne de nouvelles technologies, il n'est pas évident pour un débutant en informatique de se repérer durant son ascension
+++

Des informaticiens, qui se sont penchés sur cette problématique, élaborent des "roadmap" d'apprentissage sous forme de graphe. Ces "roadmap" permettent de situer visuellement son niveau et de voir quelles sont les technologies qu'il faut aborder par rapport à son niveau afin de progresser efficacement

+++imageFloat /images/md/892A88B4CG9C5839HCFF9E48667HBC droite
  <b>Arbre de progression 'backend'</b> : 
  <ul>
    <li><u>début</u> : niveau seconde générale SNT</li>
    <li><u>fin</u> : niveau bac +5</li>
    <li><u>sens de lecture</u> : du haut vers le bas</li>
    <li><u>commentaire</u> : les langages html/css/javascript sont abordés dans cet arbre mais ils ne doivent pas être autant approfondis qu'un développeur frontend. Le langage de programmation officiel en lycée général étant python, j'ai ajouté ci-dessous une "roadmap" d'apprentissage pour ce langage</li>
    <li><u>lien</u> : +++lien "https://roadmap.sh/backend" "site roadmap.sh"</li>
  </ul>
+++

+++imageFloat /images/md/892A88B4CG9C5839HCFF9E48667HBC
  <b>Arbre de progression 'python'</b> : 
  <ul>
    <li><u>début</u> : niveau seconde générale SNT</li>
    <li><u>fin</u> : niveau bac +2/+3</li>
    <li><u>sens de lecture</u> : du haut vers le bas</li>
    <li><u>commentaire</u> : -</li>
    <li><u>lien</u> : +++lien "https://roadmap.sh/python" "site roadmap.sh"</li>
  </ul>
+++