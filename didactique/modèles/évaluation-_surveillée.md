<h1 class="p-0 mb-1">Évaluation</h1>

<style>
  table ul{
    margin-bottom:0px;
  }
  div.h4{
    padding: 0.5rem;
    border: 1px solid black;
    display: inline-block;
  }
  div.h3{
    text-decoration: underline;
  }
</style>

<div class="h2 text-center mb-3">nom_chapitre/[num] nom_module</div>

<table class="table text-center align-middle">
  <tbody>
    <tr>
        <td colspan="10" class="text-nowrap" style="width:0"><b>Méta-données</b></td>
    </tr>
    <tr>
        <td class="text-nowrap col-1">Promotion</td><td><input class="d-print-none w-100"></td>
        <td class="text-nowrap col-1">Date</td><td><input class="d-print-none w-100"></td>
    </tr>
    <tr>
        <td class="text-nowrap col-1">Nom</td><td><input class="d-print-none w-100"></td>
        <td class="text-nowrap col-1">Prénom</td><td><input class="d-print-none w-100"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm  align-middle">
    <tbody>
    <tr>
        <td colspan="10" class="text-center text-nowrap" style="width:0"><b>Consignes</b></td>
    </tr>
    <tr>
        <td class="text-center"><b>Matériel sur le bureau</b></td>
        <td><ul>
          <li>Pas de calculatrice</li>
          <li>Téléphone portable <b>sur le bureau et éteint</b></li>
          <li>Pas de trousse</li>
        </ul></td>
    </tr>
    <tr>
        <td class="text-center"><b>Divers</b></td>
        <td><ul>
          <li>Les exercices sont indépendants</li>
          <li>Tout échange avec un autre apprenant est interdit</li>
          <li>Recopier sur un voisin : sanction pour les deux élèves concernés</li>
          <li>Au 'top' annonçant la fin de l'évaluation : <b>vous devez arrêter d'écrire et poser votre stylo</b></li>
        </ul></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
    <tbody>
    <tr>
        <td colspan="10" class="text-center text-nowrap" style="width:0"><b>Calcul de la note d'une question</b></td>
    </tr>
    <tr>
        <td class="w-0 text-center align-middle">questions de cours</td>
        <td colspan="8">Tous les points sont accordés hormis lorsque la réponse est fausse (réponse unique) ou partiellement fausse (réponses multiples) qui ne donne alors aucun point</td>
    </tr>
    <tr>
        <td rowspan="2" class="w-0 text-center align-middle">exercices</td>
        <td colspan="2" class="text-nowrap text-end">appréciation :</td>
        <td class="text-center">TP</td>
        <td class="text-center">P</td>
        <td class="text-center">B</td>
        <td class="text-center">TB</td>
    </tr>
    <tr>
        <td class="text-center"><b>calcul</b></td>
        <td class="text-nowrap text-end">nombre de points max de la question multiplié par : </td>
        <td class="text-center">0</td>
        <td class="text-center">0,3</td>
        <td class="text-center">0,7</td>
        <td class="text-center">1</td>
    </tr>
  </tbody>
</table>

+++pagebreak

<div class="h3">Questions de cours (8 points)</div>

<div class="randomizer randomizer-bloc">

  +++ds-qcm "question 1 ?" 2
    description blabla blabla
    
    reponse 1 blabla
    reponse 2 blabla
    reponse 3 blabla
  +++

  +++ds-qcm-2 "question 2 ?"
    reponse 1 blabla
    reponse 2 blabla
    reponse 3 blabla
    reponse 4 blabla
    reponse 5 blabla
    reponse 6 blabla
  +++

</div>

+++pagebreak

<div class="h3">Exercices (12 points)</div>
<br>

<div class="randomizer randomizer-bloc">

  <div>
  
    +++nobreak
    
      <div class="h4">Exercice</div>
      
      <p>description diverse</p>
    
    nobreak+++
    
    <h5><b>Question 0)</b> blablala ? (1 point)</h5>
  
    +++spoil
      blabla !
    spoil+++
    
  </div>

</div>