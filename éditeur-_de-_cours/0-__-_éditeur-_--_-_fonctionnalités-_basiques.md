# Éditeur : fonctionnalités basiques

+++ sommaire

L'éditeur utilise le langage Markdown avec certaines extensions développées sur mesure. Vous trouverez ci-dessous les fonctionnalités basiques de l'éditeur.

## Ajouter un titre

Si dans l'éditeur vous écrivez :
```markdown
# titre1
## titre2
### titre3
#### titre4
##### titre5
###### titre6
```

Cela produit le résultat suivant :

# titre1
## 1] titre2
### a) titre3
#### titre4
##### titre5
###### titre6

---

## Formater le texte

Si dans l'éditeur vous écrivez :
```markdown
**Texte en gras**
*Texte en italic*
```
Cela produit le résultat suivant : 

**Texte en gras**

*Texte en italic*

## Ajouter une liste

Si dans l'éditeur vous écrivez :
```markdown
- ceci est une liste sans numéro
    - ceci est une sous liste sans numéro
1. ceci est une liste numéroté
2. voici le deuxième élément de la liste
    42. gogo !
```
Cela produit le résultat suivant : 

- ceci est une liste sans numéro
    - ceci est une sous liste sans numéro
1. ceci est une liste numéroté
2. voici le deuxième élément de la liste
    42. gogogo !

## Ajouter un bloc de code

Si dans l'éditeur vous écrivez :

```markdown
    ```python
    #code python
    print(23 + 19)
    ```
    
    ```php
    //code php
    print(2 * 3 * 7)
    ```
    
    ```js
    // code js
    let chuckNorris = function () {
      return 42; 
    };
    
    console.log(chuckNorris()); // ne faites pas ça chez vous
    ```
```


Cela produit le résultat suivant : 

```python
#code python
print(23 + 19)
```

```php
//code php
print(2 * 3 * 7)
```

```js
// code js
let chuckNorris = function () {
  return 42; 
};

console.log(chuckNorris()); // ne faites pas ça chez vous
```

Il est bien sur possible d'utiliser d'autres langages !

## Ajouter un tableau

Si dans l'éditeur vous écrivez :

```markdown
|id|le saviez vous ?|
|--|--|
|1|Le cœur de la Terre est plus chaud que la surface du Soleil|
|2|42 est le numéro de l'appartement de Fox Mulder dans la série X-Files **COMME DE PAR HASARD** |
```

Cela produit le résultat suivant : 

|id|le saviez vous ?|
|--|--|
|1|Le cœur de la Terre est plus chaud que la surface du Soleil|
|2|42 est le numéro de l'appartement de Fox Mulder dans la série X-Files **COMME DE PAR HASARD** |

## Ajouter un lien

Les liens générés ouvrent automatiquement vers un nouvel onglet.

Si dans l'éditeur vous écrivez :

```markdown
Voici [un chouette son !](https://www.youtube.com/watch?v=4TKcz5AqcnQ)
```

Cela produit le résultat suivant : 

Voici [un chouette son !](https://www.youtube.com/watch?v=4TKcz5AqcnQ)

## Intégrer une image ou un gif

Si dans l'éditeur vous écrivez :

```markdown
![iceland](https://publicholidays.eu/wp-content/uploads/2018/03/EU_Iceland_English_2020_Output.jpg)
```

Cela produit le résultat suivant : 

![iceland](https://publicholidays.eu/wp-content/uploads/2018/03/EU_Iceland_English_2020_Output.jpg)

Il est également possible d'inclure des gifs, si dans l'éditeur vous écrivez :

```markdown
![amazing](https://i.ibb.co/1Lv4rP6/tenor-omg.gif)
```

Cela produit le résultat suivant : 

![amazing](https://i.ibb.co/1Lv4rP6/tenor-omg.gif)

## Intégrer une vidéo

Il est également possible d'intégrer des vidéos avec l'éditeur.
Si dans l'éditeur vous écrivez :

```
<div class="text-center">
<iframe src="https://player.vimeo.com/video/138623558" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
</div>
```

Cela produit le résultat suivant : 

<div class="text-center">
<iframe src="https://player.vimeo.com/video/138623558" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
</div>

## Générer un sommaire

Pour générer un sommaire, rien de plus simple !

Si dans l'éditeur vous écrivez :

`+++ sommaire`

Cela produit le résultat suivant : 

+++ sommaire

Le sommaire se génère à l'aide des titres de niveau 2 à 6.

## Générer un bloc spoil

Parfois il est utile de ne pas afficher directement une information (pour poser une question face aux élèves par exemple). Un exemple :

Exercice : combien font 2 x 5 ?

+++spoil indication
2 x 5 revient à faire 5 + 5
spoil+++

+++spoil réponse
la réponse est 10 !
spoil+++

Pour l'exemple ci-dessus, il suffit d'écrire dans l'éditeur : 

```
+++spoil indication
2 x 5 revient à faire 5 + 5
spoil+++
```

```
+++spoil réponse
la réponse est 10 !
spoil+++
```

Il est possible d'ajouter des classes css à la fin du bloc. La classe `d-print-none` permet de ne pas afficher la réponse si le document est imprimé.


## Générer une citation

Envie d'invoquer un personnage important ? les citations sont là pour ça ! 

Si dans l'éditeur vous écrivez :

```
+++ citation
blabla
+++
```

Cela produit le résultat suivant : 

+++ citation
blabla
+++

Il est possible de générer des citations plus complexes avec image, nom de l'auteur et la source de la citation. Par exemple, si dans l'éditeur vous écrivez : 

```
+++ citation https://i.ibb.co/pdKVTgp/File-source-http-commons-wikimedia-org-wiki-File-Albert-Einstein-1947-jpg.jpg
Faut pas croire tout ce qu'on voit sur le web
+++ albert einstein https://youtu.be/o-YBDTqX_ZU?t=42
```

Cela produit le résultat suivant :

+++ citation https://i.ibb.co/pdKVTgp/File-source-http-commons-wikimedia-org-wiki-File-Albert-Einstein-1947-jpg.jpg
Faut pas croire tout ce qu'on voit sur le web
+++ albert einstein https://youtu.be/o-YBDTqX_ZU?t=42

## Générer un texte avec une image à droite ou à gauche 

La flemme de coder en html/css pour afficher une image à droite ou à gauche dun texte ? cette fonctionnalité permet de répondre à ce problème ! 
Si dans l'éditeur vous écrivez : 

```
+++ imageFloat https://i.ibb.co/pdKVTgp/File-source-http-commons-wikimedia-org-wiki-File-Albert-Einstein-1947-jpg.jpg droite
Encore moi ! 
+++
```

Cela produit le résultat suivant : 


+++ imageFloat https://i.ibb.co/pdKVTgp/File-source-http-commons-wikimedia-org-wiki-File-Albert-Einstein-1947-jpg.jpg droite
Encore moi ! 
+++