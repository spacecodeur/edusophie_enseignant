# README

Ici sont rassemblés les accès aux documents/supports/... attendus dans le cadre de l'inspection.

+++bulle matthieu
  bonne lecture !
+++

+++sommaire

## Documents institutionnels

- Cahier de texte de l'année :
    - +++lien "https://www.ecoledirecte.com/CahierDeTexte" "cahier de texte d'école directe"

## Séquence / séance

- +++lien "/enseignant/inspection/progressions-_et-_programmations/NSI-_--_-_progression-_et-_programmation.md" "Programmation annuelle, la progression pédagogique"
- Présentation de l’articulation de la séance observée dans la séquence (séances en amont et aval et l’évaluation prévue)
    - +++lien "/enseignant/inspection/fiches-_sequence-_et-_seances/sequence.md" "fiche de séquence du module 8"
- Déroulement prévu de la séance
    - <del> +++lien "/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_5_projet.md" "fiche de séance 5 du module 8"</del> (covid)
    - <b>[MAJ]</b> +++lien "/enseignant/inspection/fiches-_sequence-_et-_seances/seances/seance_6_projet.md" "fiche de séance 6 du module 8"
- Supports de la séance observée, ensemble des documents distribués aux élèves
    - +++lien "/enseignant/didactique/modeles/projet/grille-_evaluation-_projet.md" "grille d'évaluation de projet" : fournie en tout début de projet, cette grille servira dans l'évaluation des élèves à l'issue du projet et de sa présentation
    - cahiers des charges
        - +++lien "/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/projets/evaluateur-_de-_mot-_de-_passe/cahier-_des-_charges.md" "projet : site ecommerce"
        - +++lien "/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table/projets/site-_ecommerce/cahier-_des-_charges.md" "projet : évaluateur de mot de passe"

- Cahiers ou classeurs d’élèves de la classe ou du niveau observé
    - à récupérer auprès des élèves

## Évaluation

- Des copies corrigées d’élèves
    - à récupérer auprès des élèves
- Des sujets d’évaluation
    - +++lien "/hidden/controles" "liste des sujets (avec correction) de SNT et de NSI" <i class="fas fa-lock text-red"></i>
- Le relevé de notes du professeur et/ou de validation des compétences pour la classe
    - +++lien "https://www.ecoledirecte.com/CarnetDeNotes" "carnet de notes d'école directe" ( <b class="text-red">/!\ </b> classe observée : 1ère NSI, groupe 1 <b class="text-red">/!\ </b>)
    
+++pagebreak

## Divers

- +++lien "/apprenant/01-__-_NSI-_premiere" "Supports de cours et activités pour le programme de NSI" 
- +++lien "/apprenant/00-__-_SNT" "Supports de cours et activités pour le programme de SNT"
- +++lien "/enseignant/inspection/progressions-_et-_programmations/SNT-_--_-_progression-_et-_programmation.md" "Progression et programmation SNT"
- +++lien "/d/nsi_1ere/plaquette.png" "Infographie de présentation de la spécialité NSI"