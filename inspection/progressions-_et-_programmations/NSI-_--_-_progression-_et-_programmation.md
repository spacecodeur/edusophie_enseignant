# NSI : progression et programmation

+++sommaire

## Liste des thèmes

<style>
  .t1{color:#67b021}
  .t2{color:#069668}
  .t3{color:#b25ea8}
  .t4{color:#081394}
  .t5{color:#932226}
  .t6{color:#444444}
  .t7{color:#e114dc}
  
  .modEven{background-color:#baffab !important;vertical-align: middle}
  .modOdd{background-color:#abfbff !important;vertical-align: middle}
  .vac{background-color:#ffabfb !important;vertical-align: middle}
  
  table td ul{
    margin-bottom:0px;
  }
  /*
  thead > tr {
    position: sticky;
    top: 0;
    background-color: #edecec;
    box-shadow: 0 4px 2px -1px rgba(0, 0, 0, 0.4);
  }
  */
  table {
    border-collapse: separate; /* Don't collapse */
    border-spacing: 0;
  }
  
</style>

<ol>
  <li class="t1">Représentation des données: types et valeurs de base</li>
  <li class="t2">Représentation des données: types construits</li>
  <li class="t3">Traitement de données en tables</li>
  <li class="t4">Interactions entre l’homme et la machine sur le Web</li>
  <li class="t5">Architectures matérielles et systèmes d’exploitation</li>
  <li class="t6">Langages et programmation</li>
  <li class="t7">Algorithmique</li>
</ol>

Le thème 'Histoire de l’informatique' étant transverse. Il n'est pas détaillé dans le tableau de la progression. 

## Progression, détaillé et articulations inter-modules

Chaque module contient un ensemble de capacités appartenant à différents thèmes. Les modules sont listés ci-dessous par ordre chronologique. 

- <b role="button" data-edusmoothscroll="#m0-down" data-decal="0" id="m0-up"><i class="text-blue fas fa-arrow-down "></i> Module 00</b> : <u>Rappels en programmation et système d'exploitation Linux</u>
    - En première NSI, la part dédiée à la programmation est importante. Le but de ce premier module est de consolider des concepts de programmation abordés en seconde. 
    - Les élèves utiliseront toute l'année un ordinateur portable sous Linux (Debian). Pour les familiariser au plus tôt avec cet environnement, ce premier module travaille donc certaines capacités ciblées du thème "<span class="t5">Architectures matérielles et systèmes d’exploitation</span>"
    - Dans ce module, le plus long de tous, je prends le temps avec les élèves pour m'assurer que nous partons sur des bases solides pour le reste de l'année
- <b role="button" data-edusmoothscroll="#m1-down" id="m1-up"><i class="text-blue fas fa-arrow-down"></i> Module 01</b> : <u>Introduction en informatique fondamentale et introduction en programmation Web côté client</u>
    - Après avoir abordés des concepts liés à l'environnement Linux, nous nous intéressons au modèle Von Neumann. Ce qui me permet de tracer un chemin direct vers le concept de portes logiques
    - Le Web permet de créer facilement des applications graphiques. C'est la raison pour laquelle, tôt dans l'année, j'initie les élèves à ce domaine. Tout au long de l'année nous réinvestirons dessus à travers notamment les projets.
- <b role="button" data-edusmoothscroll="#m2-down" id="m2-up"><i class="text-blue fas fa-arrow-down"></i> Module 02</b> : <u>Introduction aux tableaux et programmation Web côté client</u>
    - Toujours dans une optique de réinvestissement tout au long de l'année, je fais le choix d'aborder assez tôt avec les élèves le concept des tableaux en programmation
    - Nous continuons à travailler sur le Web et j'en profite pour introduire à mes élèves le langage Javascript. Les élèves devront entre autres compléter une "CheatSheet" Javascript/Python. Ce qui permettra de faire des rappels en python tout en observant les grandes ressemblances avec le Javascript
    - Enfin, un mini-projet est prévu et dans lequel les élèves devront développer une galerie de photo Web. Une bibliothèque devra être utilisée pour pouvoir zoomer sur les photos. Ce projet me permet de poser des bases de gestion de projet qui seront approfondies dans la suite de l'année : la répartition des rôles frontend/backend et la gestion de client et de serveur
- <b role="button" data-edusmoothscroll="#m3-down" id="m3-up"><i class="text-blue fas fa-arrow-down"></i> Module 03</b> : <u>Parcours de tableaux, introduction en algorithmique et tests</u>
    - Nous réinvestissons et approfondissons les tableaux en programmation. Cela me permet d'introduire des premières notions d'algorithmique aux élèves. 
    - Les programmes gagnants en complexité, nous explorons donc d'avantage les notions de qualité de code en programmation dans ce module
- <b role="button" data-edusmoothscroll="#m4-down" id="m4-up"><i class="text-blue fas fa-arrow-down"></i> Module 04</b> : <u>Introduction au réseau</u>
    - Ce module a deux objectifs ; faire le tour de la partie réseau du programme et également faire une pause sur les langages de programmation que nous travaillons tout au long des modules depuis le début de l'année
    - Afin de ne pas rester dans le pur théorique, un TP sera prévu et durant lequel les élèves simulerons des réseaux à l'aide d'un logiciel
- <b role="button" data-edusmoothscroll="#m5-down" id="m5-up"><i class="text-blue fas fa-arrow-down"></i> Module 05</b> : <u>Programmation Web et architecture client-serveur</u>
    - Ce module est essentiellement porté sur le Web
    - Nous réinvestissons des concepts travaillés précédemment dans l'année et nous nous concentrons sur le ping-pong effectué entre client et serveur à travers des formulaires
    - Ayant vu certains élèves jouer pendant les pauses inter-classes au jeu <a href="https://fr.wikipedia.org/wiki/Cookie_Clicker" target="_blank">cookie clicker</a>, Cela m'a donc donné l'idée de leur faire développer une première version de ce jeu.  
    - Un espace d'administration sera contenu dans le jeu, et dans chaque équipe un élève devra trouver le moyen d'obtenir le mot de passe pour y accéder (ce qui permettra de mettre en lumière l'importance des transmissions chiffrées dans un réseau)
- <b role="button" data-edusmoothscroll="#m6-down" id="m6-up"><i class="text-blue fas fa-arrow-down"></i> Module 06</b> : <u>Recherche de données dans une table, systèmes binaire et hexadécimal</u>
    - Dans ce module nous nous intéressons au format de fichier de données structurées qu'est le CSV. J'en profite pour refaire quelques rappels sur les architectures client-serveur dans la mesure où ce type de fichier permet d'échanger des données entre les différentes machines appartenant à ce type d'architecture
    - Nous mettrons également en lumière les limites que peuvent rencontrer les logiciels type tableur (fichier de données trop volumineux, fonctionnalités de recherche limitées). Ce qui permettra de continuer à travailler la programmation Python
    - Nous repassons ensuite dans le domaine de l'informatique fondamentale en nous intéressons à la représentation des nombres décimaux côté machine. Ce qui permet de faire un premier pas vers les mathématiques
- <b role="button" data-edusmoothscroll="#m7-down" id="m7-up"><i class="text-blue fas fa-arrow-down"></i> Module 07</b> : <u>Algorithmes de recherche, de tri et invariant de boucle</u>
    - Nous abordons dans ce module les concepts qui, je pense, sont les plus difficiles à appréhender pour les élèves cette année. Les élèves doivent avoir un bagage en programmation solide, les mathématiques sont également très sollicités dans ce module. Le contenu de ce module ne pouvait donc être entamé en début d'année et le placer maintenant permet d'avoir une bonne marge de manoeuvre pour le retravailler si besoin d'ici la fin de l'année 
    - Dans un premier temps nous travaillons sur des algorithmes simples (min, max, moyenne), ce qui permet de faire des rappels sur des concepts importants de programmation (les boucles, les tableaux...) 
    - Les algorithmes de tri par insertion et par sélection sont assez semblables. Ainsi, une séance est consacrée à la programmation de l'algorithme de tri par insertion et à l'étude de sa complexité. Les élèves devront s'appuyer sur cette séance pour refaire la même chose à la maison mais pour l'algorithme de tri par sélection (qui est un peu plus simple)
    - Pour terminer, nous nous intéresserons à la validité d'un algorithme et à sa terminaison sur des exemples simples. Les élèves devront entre autres démontrer la terminaison des algorithmes de tri que nous avons étudiés
- <b role="button" data-edusmoothscroll="#m8-down" id="m8-up"><i class="text-blue fas fa-arrow-down"></i> Module 08</b> : <u>Représentation des entiers relatifs et réels en binaire et tri et fusion de données dans une table</u>
    - Nous réinvestissons sur les concepts de représentation de nombre binaire côté machine et nous en profitons pour aller un peu plus loin : comment une machine traite des nombres négatifs et des nombres réels ?
    - Nous continuons également le travail initié sur les fichiers CSV. Entre temps nous avons abordés les algorithmes de tri, donc nous allons jouer avec les deux et voir comment trier des données en table
    - Un projet de 3 à 5 séances permettra aux élèves de mettre en application la manipulation de table (dans un fichier CSV) et de travailler divers concepts étudiés depuis le début de l'année. Un ensemble de projet sera proposé aux élèves. Les élèves, prévenus en début de module, pourront eux même proposer leurs idées de projets avec comme contrainte que ce dernier doit utiliser et manipuler un fichier CSV
- <b role="button" data-edusmoothscroll="#m9-down" id="m9-up"><i class="text-blue fas fa-arrow-down"></i> Module 09</b> : <u>Tableaux à 2 dimensions et systèmes d'encodage</u>
    - Avant d'entamer la dernière partie du programme dédiée aux algorithmes, il est nécessaire d'initier les élèves aux tableaux de deux dimensions, les séances consacrées à cela permettront de continuer à s'entraîner à programmer en python
    - Nous terminons également dans ce module notre tour d'horizon sur la représentation des données côté machine avec les notions d'encodage de caractère
    - Des activités python puiseront sur ces deux points (table d'encodage = tableau de deux dimensions)
- <b role="button" data-edusmoothscroll="#m10-down" id="m10-up"><i class="text-blue fas fa-arrow-down"></i> Module 10</b> : <u>Tableaux associatifs, algorithmes d'apprentissage et algorithmes gloutons</u>
    - Les dictionnaires (ou tableaux associatifs) sont une structure récurrente en programmation. Nous ne l'abordons que maintenant car nous n'en avons pas eu besoin jusqu’alors. Un réinvestissement sera fait avec le traitement de fichier CSV (le contenu d'un fichier CSV pourra être stocké dans un dictionnaire). Un comparatif tableaux / dictionnaire sera également fait
    - Nous nous ré-intéressons à l'algorithmique. Ce sera l'occasion de réinvestir beaucoup de concepts importants vus pendant l'année : la complexité, la programmation, ...
- <b role="button" data-edusmoothscroll="#m11-down" id="m11-up"><i class="text-blue fas fa-arrow-down"></i> Module 11</b> : <u>Module de fin d'année</u>
    - Ce module de fin d'année est consacré à un à plusieurs projets qui s'appuieront sur les concepts travaillées pendant tout l'année
    - Des idées de projet seront proposés mais les élèves seront fortement encouragés à proposer les leurs

## Programmation

<table class="table">
<thead>
<tr>
    <th scope="col" class="text-vertical px-0 text-center">semaine</th>
    <th scope="col" class="text-vertical px-0 text-center">séance</th>
    <th class="w-100 align-middle text-center" scope="col">contenu</th>
</tr>
</thead>
<tbody>
  <tr id="m0-down"><td class="modEven" colspan="3"><div><b role="button" data-edusmoothscroll="#m0-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 00 ]</b> : Rappels en programmation et système d'exploitation Linux ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/00-__-_rappels-_en-_programmation-_et-_systeme-_d--__exploitation-_Linux"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>36</td><td>0</td>
    <td>
    <ul>
      <li class="t6">Mettre en évidence un corpus de constructions élémentaires (les variables)</li>
    </ul></td>
  </tr>
  <tr><td>36</td><td>1</td>
    <td><ul>
      <li class="t6">Mettre en évidence un corpus de constructions élémentaires (les instructions conditionnelles)</li>
    </ul></td>
  </tr>
    <tr><td>37</td><td>2</td>
    <td><ul>
      <li class="t5">Identifier les fonctions d’un système d’exploitation</li>
      <li class="t5">Utiliser les commandes de base en ligne de commande</li>
    </ul></td>
  </tr><tr><td>37</td><td>3</td>
    <td><ul>
      <li class="t6">Mettre en évidence un corpus de constructions élémentaires (les instructions itératives)</li>
    </ul></td>
  </tr>
  <tr><td>38</td><td>4</td>
    <td><ul>
      <li class="t5">Gérer les droits et permissions d’accès aux fichiers</li>
      <li class="t6">Prototyper une fonction</li>
      <li class="t6">Décrire les préconditions sur les arguments</li>
    </ul></td>
  </tr><tr><td>38</td><td>5</td>
    <td><ul>
      <li class="t6">Prototyper une fonction</li>
      <li class="t6">Décrire les préconditions sur les arguments</li>
    </ul><div><i>+ exercices de programmation pendant une heure pour préparer le contrôle de la séance suivante</i></div>
    </td>
  </tr>
  <tr><td>39</td><td>6</td>
    <td>évaluation sommative (1h) : <b>[ Module 00 ]</b></td>
  </tr>
  <tr id="m1-down"><td class="modOdd" colspan="3"><div><b role="button" data-edusmoothscroll="#m1-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 01 ]</b> : Introduction en informatique fondamentale et introduction en programmation Web côté client ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>39</td><td>0</td>
    <td><ul>
      <li class="t5">Distinguer les rôles et les caractéristiques des différents constituants d’une machine</li>
      <li class="t1">Dresser la table d’une expression booléenne</li>
    </ul></td>
  </tr>
  <tr><td>40</td><td>1</td>
    <td><ul>
      <li class="t4">Identifier les différents composants graphiques permettant d’interagir avec une application Web</li>
      <li class="t4">Identifier les événements que les fonctions associées aux différents composants graphiques sont capables de traiter</li>
    </ul></td>
  </tr>
  <tr id="m2-down"><td class="modEven" colspan="3"><div><b role="button" data-edusmoothscroll="#m2-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 02 ]</b> : Introduction aux tableaux et programmation Web côté client ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/02-__-_introduction-_aux-_tableaux-_et-_programmation-_Web-_cote-_client"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>40</td><td>0</td>
    <td><ul>
      <li class="t2">Lire et modifier les éléments d’un tableau grâce à leurs index</li>
    </ul></td>
  </tr>
  <tr><td>41</td><td>-</td>
    <td>évaluation sommative (1h) : <b>[ Modules ≤ 01 ]</b></td>
  </tr>
  <tr><td>41</td><td>1</td>
    <td><ul>
      <li class="t4">Analyser et modifier les méthodes exécutées lors d’un clic sur un bouton d’une page Web</li>
      <li class="t6">Repérer, dans un nouveau langage de programmation,les traits communs et les traits particuliers à ce langage</li>
    </ul></td>
  </tr>
  <tr><td>42</td><td>2</td>
    <td class="align-middle" rowspan="2">Projet : <b>[ Modules ≤ 02 ]</b>
      <ul class="text-start mb-0"><li class="t6">Utiliser la documentation d’une bibliothèque</li></ul></td>
  </tr>
  <tr><td>42</td><td>3</td></tr>
  <tr><td colspan="3" class="vac"><b>Vacances de Toussaint</b></td></tr>
  <tr id="m3-down"><td class="modOdd" colspan="3"><div><b role="button" data-edusmoothscroll="#m3-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 03 ]</b> : Parcours de tableaux, introduction en algorithmique et tests ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/03-__-_parcours-_de-_tableaux---_-_introduction-_en-_algorithmique-_et-_tests"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>45</td><td>0</td>
    <td>
    <ul>
      <li class="t2">Itérer sur les éléments d’un tableau</li>
      <li class="t7">Écrire un algorithme de recherche d’une occurrence sur des valeurs de type quelconque</li>
    </ul></td>
  </tr>
  <tr><td>45</td><td>1</td>
    <td><ul>
      <li class="t6">Décrire des postconditions sur les résultats</li>
      <li class="t6">Utiliser des jeux de tests</li>
    </ul></td>
  </tr>
  <tr><td>46</td><td>2</td>
    <td>évaluation sommative (1h) : <b>[ Modules ≤ 03 ]</b></td>
  </tr>
  <tr id="m4-down"><td class="modEven" colspan="3"><div><b role="button" data-edusmoothscroll="#m4-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 04 ]</b> : Introduction au réseau ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/04-__-_introduction-_au-_reseau"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>46</td><td>0</td>
    <td><ul>
      <li class="t5">Mettre en évidence l’intérêt du découpage des données en paquets et de leur encapsulation</li>
      <li class="t5">Dérouler le fonctionnement d’un protocole simple de récupération de perte de paquets (bit alterné)</li>
      </ul>
    </td>
  </tr>
  <tr><td>47</td><td>1</td>
    <td><ul>
      <li class="t5">Simuler ou mettre en œuvre un réseau</li>
    </ul></td>
  </tr>
  <tr id="m5-down"><td class="modOdd" colspan="3"><div><b role="button" data-edusmoothscroll="#m5-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 05 ]</b> :  Programmation Web et architecture client-serveur ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/05-__-_programmation-_Web-_et-_architecture-_client-_serveur"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>47</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Analyser le fonctionnement d’un formulaire simple</li>
      <li class="t4">Distinguer les transmissions de paramètres par les requêtes POST ou GET</li>
    </ul></td>
  </tr>
  <tr><td>48</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre</li>
      <li class="t4">Distinguer ce qui est mémorisé dans le client et retransmis au serveur</li>
    </ul></td>
  </tr>
  <tr><td>48</td><td>2</td>
    <td class="align-middle" rowspan="3">Projet évalué : <b>[ Modules ≤ 05 ]</b>
      <ul class="text-start mb-0"><li class="t4">Reconnaître quand et pourquoi la transmission est chiffrée</li></ul></td>
  </tr><tr><td>49</td><td>3</td>
  </tr><tr><td>49</td><td>4</td></tr>
  <tr><td>50</td><td>X</td>
    <td class="align-middle" rowspan="2">~ séance(s) tampon(s) du trimestre ~</td>
  </tr>
  <tr><td>50</td><td>X</td>
  </tr>
  <tr><td colspan="6" class="vac"><b>Vacances de Noël</b></td></tr>
  <tr><td>1</td><td>?</td>
    <td>Présentations orales des projets</td>
  </tr>
  <tr id="m6-down"><td class="modEven" colspan="3"><div><b role="button" data-edusmoothscroll="#m6-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 06 ]</b> :  Recherche de données dans une table, systèmes binaire et hexadécimal ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/06-__-_recherche-_de-_donnees-_dans-_une-_table---_-_systemes-_binaire-_et-_hexadecimal"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>1</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t3">Importer une table depuis un fichier texte tabulé ou un fichier CSV</li>
      <li class="t3">Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle 1/2 : capacité étalée sur deux séances pour consacrer du temps à réinvestir sur les tableaux en python</li>
    </ul></td>
  </tr>
  <tr><td>2</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t3">Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle 2/2</li>
    </ul></td>
  </tr>
  <tr><td>2</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t1">Passer de la représentation d’une base dans une autre</li>
      <li class="t1">Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entier</li>
    </ul></td>
  </tr>
  <tr><td>3</td><td>3</td>
    <td>évaluation sommative (1h) : <b>[ Modules ≤ 06 ]</b></td>
  </tr>
  <tr id="m7-down"><td class="modOdd" colspan="3"><div><b role="button" data-edusmoothscroll="#m7-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 07 ]</b> : Algorithmes de recherche, de tri et invariant de boucle ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/07-__-_algorithmes-_de-_recherche---_-_de-_tri-_et-_invariant-_de-_boucle"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>3</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t7">Écrire un algorithme de recherche d’un extremum, de calcul d’une moyenne</li>
    </ul></td>
  </tr>
  <tr><td>4</td><td>1</td>
    <td rowspan="2" class="align-middle"><ul class="text-start mb-0">
      <li class="t2">Écrire une fonction renvoyant un p-uplet de valeurs</li>
      <li class="t7">Écrire un algorithme de tri (insertion et sélection)</li>
    </ul></td>
  </tr>
  <tr><td>4</td><td>2</td>
  </tr>
  <tr><td>5</td><td>3</td>
    <td><ul class="text-start mb-0">
      <li class="t7">Décrire un invariant de boucle qui prouve la correction des tris par insertion, par sélection</li>
    </ul></td>
  </tr>
  <tr><td>5</td><td>4</td>
    <td>séance d'approfondissement sur les algorithmes, la validité et la terminaison</td>
  </tr>
  <tr id="m8-down"><td class="modEven" colspan="3"><div><b role="button" data-edusmoothscroll="#m8-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 08 ]</b> : Représentation des entiers relatifs et réels en binaire et tri et fusion de données dans une table ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/08-__-_representation-_des-_entiers-_relatifs-_et-_reels-_en-_binaire-_et-_tri-_et-_fusion-_de-_donnees-_dans-_une-_table"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>6</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t1">Utiliser le complément à 2</li>
      <li class="t1">Calculer sur quelques exemples la représentation de nombres réels : 0.1, 0.25 ou 1/3</li>
    </ul></td>
  </tr>
  <tr><td>6</td><td>-</td>
    <td>séance exercices pour le contrôle trimestriel de la semaine suivante</td>
  </tr>
  <tr><td>7</td><td>-</td>
    <td>évaluation sommative (2h) : <b>[ Modules ≤ 07 ]</b></td>
  </tr>
  <tr><td>7</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t3">Trier une table suivant une colonne</li>
      <li class="t3">Construire une nouvelle table en combinant les données de deux tables</li>
    </ul></td>
  </tr>
  <tr><td colspan="6" class="vac"><b>Vacances de Février</b></td></tr>
  <tr><td>10</td><td>X</td>
    <td class="align-middle" rowspan="2">~ séance(s) tampon(s) du trimestre ~</td>
  </tr>
  <tr><td>10</td><td>X</td></tr>
  <tr><td>11</td><td>?</td>
    <td class="align-middle" rowspan="3">Projet évalué : <b>[ Modules ≤ 08 ]</b>
  </td></tr>
  <tr><td>11</td><td>?</td></tr>
  <tr><td>12</td><td>?</td></tr>
  <tr id="m9-down"><td class="modOdd" colspan="3"><div><b role="button" data-edusmoothscroll="#m9-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 09 ]</b> :  Tableaux à 2 dimensions et systèmes d'encodage ( <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/09-__-_tableaux-_a-_2-_dimensions-_et-_systemes-_d--__encodage"><i class="fa fa-folder isDirectory"></i></a> )</div>
  </td></tr>
  <tr><td>12</td><td>0</td>
    <td rowspan="2" class="align-middle"><ul class="text-start mb-0">
      <li class="t2">Utiliser des tableaux de tableaux pour représenter des matrices : notation a [i] [j]</li>
    </ul></td>
  </tr>
  <tr><td>13</td><td>1</td>
  </tr>
  <tr><td>13</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t1">Identifier l’intérêt des différents systèmes d’encodage</li>
      <li class="t1">Convertir un fichier texte dans différents formats d’encodage</li>
    </ul></td>
  </tr>
  <tr><td>14</td><td>3</td>
    <td>évaluation sommative (1h) : <b>[ Modules ≤ 09 ]</b></td>
  </tr>
  <tr id="m10-down"><td class="modEven" colspan="3"><div><b role="button" data-edusmoothscroll="#m10-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 10 ]</b> : Tableaux associatifs, algorithmes d'apprentissage et algorithmes glouton </div>
  </td></tr>
  <tr><td>14</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t2">Construire une entrée de dictionnaire</li>
      <li class="t2">Itérer sur les éléments d’un dictionnaire</li>
    </ul></td>
  </tr>
  <tr><td>15</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t7">Écrire un algorithme qui prédit la classe d’un élément en fonction de la classe majoritaire de ses k plus proches voisin</li>
    </ul></td>
  </tr>
  <tr><td>15</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t7">Résoudre un problème grâce à un algorithme glouton</li>
    </ul></td>
  </tr>
  <tr><td>16</td><td>3</td>
    <td>évaluation sommative (1h) : <b>[ Modules ≤ 10 ]</b></td>
  </tr>
  <tr id="m11-down"><td class="modOdd" colspan="3"><div><b role="button" data-edusmoothscroll="#m11-up">[ <i class="text-blue fas fa-arrow-up"></i> Module 11 ]</b> : Module de fin d'année</div>
  </td></tr>
  <tr><td>16</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="">?</li>
      <li class="">?</li>
    </ul></td>
  </tr>
  <tr><td colspan="6" class="vac"><b>Vacances de Printemps</b></td></tr>
  <tr><td>19</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="">?</li>
      <li class="">?</li>
    </ul></td>
  </tr>
  <tr><td>19</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="">?</li>
      <li class="">?</li>
    </ul></td>
  </tr>
  <tr><td>20</td><td>3</td>
    <td><ul class="text-start mb-0">
      <li class="">?</li>
      <li class="">?</li>
    </ul></td>
  </tr>
  <tr><td>20</td><td>4</td>
    <td><ul class="text-start mb-0">
      <li class="">?</li>
      <li class="">?</li>
    </ul></td>
  </tr>
  <tr><td>21</td><td>5</td>
    <td><ul class="text-start mb-0">
      <li class="">?</li>
      <li class="">?</li>
    </ul></td>
  </tr>
  <tr><td>21</td><td>6</td>
    <td><ul class="text-start mb-0">
      <li class="">?</li>
      <li class="">?</li>
    </ul></td>
  </tr>
  <tr><td>22</td><td>X</td>
    <td class="align-middle" rowspan="2">~ séance(s) tampon(s) du trimestre ~</td>
  </tr>
  <tr><td>22</td><td>X</td></tr>
</tbody>
</table>

## Calcul du ratio projet / cours 

- nombre semaines totales : 31
    - nombre semaines cours + évaluations : 19,5
    - nombre semaines projets : 5
    - nombre semaines tampons : 3
    - nombre de semaines du module de fin d'année : 3.5 
- Les séances du module de fin d'année sont réservées à un à plusieurs projets 
    - sans les séances de fin d'année, nous obtenons un ratio de 5 / 27.5 ~= 18% de temps consacré à des projets
    - en prenant en compte les séances de fin d'année, nous obtenons un ratio de 8.5 / 31 ~= 27% de temps consacré à des projets 
    - si aucune séance tampon n'a été consommée, nous obtenons un ratio de 11.5 / 31 ~= 37% de temps consacré à des projets 

En conclusion, **le temps alloué pour des projets** varie entre 18% et 37% en fonction des aléas rencontrés pendant l'année

## Modification en cours d'année de la programmation

<table>
  <thead>
    <tr class="text-center">
      <th>Date</th>
      <th>Description</th>
      <th>Modification</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>14/09/2021</td>
      <td><ul>
        <li>les aléas de la rentrée et le niveau des élèves ont mal été anticipés. Le module 00 ne s'est pas déroulé en deux semaines comme prévu initialement</li>
        <li>Comme les sujets sont importants et seront récurent tout au long de l'année, je préfère donc prendre plus de temps dans ce module pour faire le tour des notions principales</li>
      </ul></td>
      <td><ul>
        <li>La durée du module 00 a été modifié de 2 semaines à 3 semaines (soit deux séances de 2 heures supplémentaires) </li>
        <li>En contrepartie, le temps alloué aux projets avant noël a été réduit</li>
      </ul></td>
    </tr>
    <tr>
      <td>11/11/2021</td>
      <td><ul>
        <li>Ajout de la progression de début janvier à fin Mai</li>
      </ul></td>
      <td><ul>
        <li>La progression janvier à fin Mai, conçue avant septembre, est maintenant posée en détail dans ce document</li>
        <li>Des statistiques sur l'ensemble de l'année ont été ajoutées dans "État des lieux de la programmation". J'ai laissé les anciennes projections (rédigées avant septembre) en dessous de cette partie</li>
      </ul></td>
    </tr>
    <tr>
      <td>25/11/2021</td>
      <td><ul>
        <li>Suppression de la séance 1 module 5 (évaluation module 4)</li>
      </ul></td>
      <td><ul>
        <li>Le but est de laissez davantage de temps pour le gros projet de décembre dans lequel sont  évaluées les capacités du module 4</li>
        <li>Le jour est ajouté dans le capital "séance tampon du trimestre"</li>
      </ul></td>
    </tr>
    <tr>
    <td>22/12/2021</td>
      <td><ul>
        <li>Ajout d'une séance supplémentaires pour le projet 'clicker game', on passe de 2 à 3 séances (sans compter la séance de présentations des projets) car plus volumineux que prévu</li>
      </ul></td>
      <td><ul>
        <li>Pas de modification pour l'instant dans le planning général, la séance supplémentaire est amortie avec une "séance tampon du trimestre"</li>
      </ul></td>
    </tr>
    <tr>
    <td>09/01/2022</td>
      <td><ul>
        <li>Dans la progression actuelle, la capacité "Utiliser le complément à 2" est abordée avant la capacité "Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entiers" (du thème : "Représentation des données: types et valeurs de base"). Or le complétment à 2 suppose que les élèves sachent déjà additionner des nombres binaires entre eux</li>
      </ul></td>
      <td><ul>
        <li>J'inverse les deux capacités dans la progression, ce qui fait plus sens</li>
      </ul></td>
    </tr>
    <tr>
    <td>29/01/2022</td>
      <td><ul>
        <li>Les évaluations pour le module 7 et 8 sont très proches (une semaine d'écart entre les deux). De plus le lycée Beauséjour mets en place des contrôles trimestriels en semaine 7.</li>
      </ul></td>
      <td><ul>
        <li>Je retire de ma progression le contrôle initialement prévu en première séance de la semaine 6 pour le remplacer par une séance d'exercice en vue de la préparation du contrôle trimestriel. Cette séance me permettra aussi d'évaluer oralement les personnes qui étaient absentes lors du projet de Noël. Le contrôle en semaine 7, qui s'étalera sur 2 heures, évaluera les modules 0 à 8 (en prio, les modules 7 et 8)</li>
      </ul></td>
    </tr>
    <tr>
    <td>06/02/2022</td>
      <td><ul>
        <li>Le module 7, consacré aux algorithmes de tri / validité / terminaison, est une partie difficile du programme. Nous avons pris d'avantage de temps dans ce module qu'initialement prévu. Plus de temps a été nécessaire pour les activités et leur correction</li>
      </ul></td>
      <td><ul>
        <li>Les temps pris ça et là ont ajouté l'équivalent d'une séance dans le module</li>
      </ul></td>
    </tr>
    <tr>
    <td>20/02/2022</td>
      <td><ul>
        <li>La manipulation de double boucles imbriquées restent difficile pour certains élèves</li>
      </ul></td>
      <td><ul>
        <li>Le prochain cours portant sur la manipulation de tableaux est le cours dédié aux tableaux à deux dimensions. Ce cours initialement prévu sur une séance est rallongé d'une séance pour prendre plus de temps sur les doubles boucles imbriquées</li>
      </ul></td>
    </tr>
    <tr><td>16/03/2022</td>
      <td><ul>
        <li>Semaine 11 : maladie, donc décalage des séances ?</li>
      </ul></td>
      <td><ul>
        <li>après réflexions : je fais le choix de garder le planning tel quel en considérant qu'il s'applique dans un cas idéal (donc en faisant abstraction des décalages éventuels de séances pour causes maladie, ...)</li>
      </ul></td>
    </tr>
  </tbody>
</table>