# Fiche de suivi de projet

Ce support est à compléter tout au long de l'avancée du projet. Certaines questions appuient sur des points du projet qui vous permettront d'avancer. 

La plupart des questions peuvent être complétées en groupe. Certaines questions demandent une réponse individuelle et personnelle et portent donc la notification **⚠️ réponse individuelle et personnelle ⚠️**. Pour ces questions, **des mêmes reponses entre élèves engeandrera de fortes pénalités dans la notation finale**.

**Ce dossier devra être fournis à votre enseignant avant la présentation des projets et servira dans votre évaluation individuelle finale.**

<div class="p-3 mb-2 bg-danger text-white d-print-none">Attention ! ce document est préparé en vue de l'oral 2 du concours de l'enseignement de SNT/NSI dans le secondaire. <span class="text-blue">Les notes en bleu</span> ne sont pas destinées aux élèves et ne figureront donc pas sur le dossier final fournis à l'élève</div>

## Phases du projet

### Phase 1

Pour concevoir la base de données de l'application, nous devons déjà savoir quelles données il va falloir y stocker et donc, quelles données sont collectées par les modules 'sensor hat' et 'nova SDS011'.

1) Lorsque vous vous connectez sur le raspberry (voir "Environnement technique" dans le cahier des charges), quelles informations très utiles pour la suite s'affichent dans le message de bienvenue ?

<div class="border border-dark mb-3 p-2">
    <p class="text-blue d-print-hidden">Le message de bienvenue explique comment lister les commandes disponibles. Il est également écrit comment obtenir la description d'une commande via le paramètre -h ou -help ou la commande man</p>
</div>

Nous considérons pour l'instant que l'application n'est prévue que pour une seule station et rappelons que le cahier des charges impose la création d'une table par module (spécification techniques > contrainte technique). 

2) Après avoir trouvé les informations nécessaires sur le raspberry, complétez les dictionnaires de données suivants : 

<div class="border border-dark mb-3 p-2" style="min-height:300px">
    <table class="table">
        <thead>
            <tr>
                <th colspan="4" class="text-center">Entité sensorhat</th>
            </tr>
            <tr>
                <th>nom</th>
                <th>description</th>
                <th>domaine</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><span class="text-blue d-print-hidden">date</span></td>
                <td><span class="text-blue d-print-hidden">date de la collecte de la donnée (yyyy-mm-dd hh:mm)</span></td>
                <td><span class="text-blue d-print-hidden">datetime</span></td>
            </tr>
            <tr>
                <td><span class="text-blue d-print-hidden">temperature</span></td>
                <td><span class="text-blue d-print-hidden">°C</span></td>
                <td><span class="text-blue d-print-hidden">float</span></td>
            </tr>
            <tr>
                <td><span class="text-blue d-print-hidden">pression</span></td>
                <td><span class="text-blue d-print-hidden">Pa</span></td>
                <td><span class="text-blue d-print-hidden">float</span></td>
            </tr>
            <tr>
                <td><span class="text-blue d-print-hidden">humidite</span></td>
                <td><span class="text-blue d-print-hidden">%</span></td>
                <td><span class="text-blue d-print-hidden">float<s pan=""></s></span></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="table">
        <thead>
            <tr>
                <th colspan="4" class="text-center">Entité sds011</th>
            </tr>
            <tr>
                <th>nom</th>
                <th>description</th>
                <th>domaine</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><span class="text-blue d-print-hidden">date</span></td>
                <td><span class="text-blue d-print-hidden">date de la collecte de la donnée (yyyy-mm-dd hh:mm)</span></td>
                <td><span class="text-blue d-print-hidden">datetime</span></td>
            </tr>
            <tr>
                <td><span class="text-blue d-print-hidden">pm25</span></td>
                <td><span class="text-blue d-print-hidden">decimal</span></td>
                <td><span class="text-blue d-print-hidden">float</span></td>
            </tr>
            <tr>
                <td><span class="text-blue d-print-hidden">pm10</span></td>
                <td><span class="text-blue d-print-hidden">decimal</span></td>
                <td><span class="text-blue d-print-hidden">float</span></td>
            </tr>
        </tbody>
    </table>
</div>

3) Rappeller la définition d'une clé primaire

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Une clé primaire est un ensemble de champs unique et non null permettant d'identifier chaque entrées dans une table</span>
</div>

4) Complétez le shéma relationnel suivant 

<div class="border border-dark mb-3 p-1 pb-3">
    sensorhat( <span class="d-print-hidden text-blue"><u>date</u>, température, pression, humidite )</span>
    <br>
    sds011(<span class="d-print-hidden text-blue"><u>date</u>, pm25, pm10 )</span>
</div>

Considérons à présent que l'application peut gérer un nombre indéfini de stations 'air quality control'. Une nouvelle entité est introduite, il s'agis de l'entité 'station' qui comporte un identifiant et le nom du lieu où elle est placée.

Afin que l'utilisateur puisse consulter des données en fonction d'une station, il doit pouvoir sélectionner en premier lieu la station souhaitée. Cela implique que l'utilisateur s'attend à voir affichée la liste de toutes les stations disponibles. 

5) Rappeller la définition d'une clé étrangère

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Une clé étrangère est un champ d'une table faisant référence à une ou plusieurs colonnes d’une autre table (représentant une clé primaire). Les colonnes référencées doivent pré-exister dans la table référencée</span>
</div>

6) Mettez à jour le shéma relationnel de la question 5 afin de pouvoir prendre en compte les données relevées en fonction de différentes station 'air quality control'

<div class="border border-dark mb-3 p-1 pb-3">
    sensorhat( <span class="d-print-hidden text-blue"><u>date</u>, température, pression, humidite, <span class="font-italic">station_id</span> )</span>
    <br>
    sds011( <span class="d-print-hidden text-blue"><u>date</u>, pm25, pm10, <span class="font-italic">station_id</span> )</span>
    <br>
    station( <span class="d-print-hidden text-blue"><u>id</u>, lieu )</span>
</div>

Pour rappel, l'utilisateur doit pouvoir lister les stations disponibles. Et il doit pouvoir consulter les données collectées depuis 24 heures selon une station donnée.

7) Concernant cette fonctionnalité obligatoire, expliquez avec vos mots quelles données doit on extraire de la base de données

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">Depuis la base de données, il faut extraire toutes les informations de toutes les stations. Il faudra également extraire toutes les données des deux modules en fonction de l'id de la station concernée</span>
</div>

8) Rédigez (et testez bien sur !) les deux requêtes dont aura besoin la future application pour pouvoir extraires les données nécessaires pour la fonctionnalité obligatoire

<div class="border border-dark mb-3 p-1 pb-3">
    <span class="d-print-hidden text-blue">SELECT * FROM station;</span>
    <br>
    <span class="d-print-hidden text-blue">
        SELECT * FROM sensorhat
        <br>
        JOIN sds011 ON sensorhat.date = sds011.date
        <br>
        WHERE station_id = 1;
    </span>
</div>

### Phase 2

**⬇️ partie en cours de rédaction ⬇️**

**⬆️ partie en cours de rédaction ⬆️**

### Phase 3

**⬇️ partie en cours de rédaction ⬇️**

**⬆️ partie en cours de rédaction ⬆️**

1) Dans le cahier des charges (section "Spécifications techniques" > "Bonnes pratiques") il est écrit : "Concentrer tous le code dans un même endroit n'est jamais une bonne idée.". Pouvez vous expliquer quels risques peuvent survenir lorsque beaucoup de code est concentré dans un même fichier ? 

**⚠️ réponse individuelle et personnelle ⚠️**

<div class="border border-dark mb-3" style="height:200px"></div>

## Notes personnelles

<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">
<hr class="mb-5 mt-5" style="height:2px;border-width:0;color:gray;background-color:gray">