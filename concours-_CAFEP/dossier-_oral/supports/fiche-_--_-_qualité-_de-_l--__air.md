# fiche : qualité de l'air

La qualité de l'air est depuis de nombreuses années un enjeu majeur de santé publique. Cette qualité est grandement impactée par l'activité humaine. Le but de cette fiche est de comprendre ce qui définie la qualité de l'air en France, et par conséquent, de comprendre les données qui seront manipulées à travers le projet "air quality control".

Pour chaque questions ci-dessus, vous devez remplir à la maison le premier bloc "à remplir à la maison" et laisser le bloc "à compléter en classe" vide. Je vous conseille de lire toutes les questions avant de démarrer vos recherches.

<b>Des élèves seront aléatoirement tirés au sort pour exposer oralement le résultat de leurs recherches.</b> Il est donc important de comprendre ce que vous décidez de noter à la maison afin de le partager le plus naturellement possible en classe.

## Les questions 

**1)** Donner une définition générale de ce qu'est la qualité de l'air.

<p class="mb-1"><b>À remplir à la maison</b></p>
<div class="border border-dark mb-3" style="height:200px"></div>

<p class="mb-1"><b>À compléter en classe</b></p>
<div class="border border-dark mb-3" style="height:200px"></div>

**2)** Lister quelques critères physiques sur lesquels se base la qualité de l'air **en France**. Pour vous aider, je vous invite à recouper les informations trouvées sur internet et les informations contenues dans le cahier des charges du projet.

<p class="mb-1"><b>À remplir à la maison</b></p>
<div class="border border-dark mb-3" style="height:200px"></div>

<p class="mb-1"><b>À compléter en classe</b></p>
<div class="border border-dark mb-3" style="height:200px"></div>

**3)** Trouver et donner une définition des critères listés dans la question précédente. Expliquer très succinctement les liens avec la qualite de l'air 

<p class="mb-1"><b>À remplir à la maison</b></p>
<div class="border border-dark mb-3" style="height:200px"></div>

<p class="mb-1"><b>À compléter en classe</b></p>
<div class="border border-dark mb-3" style="height:200px"></div>

**4)** Recherchez une lois Française publiée et en lien avec la qualité de l'air. Noter sa date de publication et expliquer en 2/3 lignes le but de la lois en question.

<p class="mb-1"><b>À remplir à la maison</b></p>
<div class="border border-dark mb-3" style="height:200px"></div>